Installation
------------

To print the parts, this requires you have some Lisp installed. If not, use
something like ``apt install sbcl``, ``pacman -S sbcl`` or ``brew install
sbcl`` to correct this deficiency.

To print the scores in pdf, you need to have the utility `ps2pdf` installed. Check if it is not already installed with `which ps2pdf` and if not, install ghostscript (set of utilities including `ps2pdf`) using you favorite package manager `apt install ghostscript`, `pacman -S ghostscript` or `brew install ghostcript`.

On UNIX distros, you may have to install timidity for playback:

```bash
sudo apt install timidity
```

In a virtualenv:

```bash
pip install -r requirements.txt
```


Test
----

```bash
./mitex examples/sample.mtx -p -s
```