import os
import cl4py
import subprocess
import multiprocessing as mp




class Printer:
	def __init__(self, path):
		self.path = path

		self.overall_part_attributes = tuple()
		self.overall_part_attributes += (("all-output-in-one-file", "t"),) # [twiby]: to check if works
		self.overall_part_attributes += (("page-height", 11.7),)
		self.overall_part_attributes += (("page-width", 8.3),)
		self.overall_part_attributes += (("size", 20),)
		self.overall_part_attributes += (("note-head-size", 1.2),)
		self.overall_part_attributes += (("free-expansion-factor", 2.5),)
		self.overall_part_attributes += (("title-separation", "3"),)
		self.overall_part_attributes += (("automatic-page-numbers", "t"),)
		self.overall_part_attributes += (("first-measure-number", 1),)
		self.overall_part_attributes += (("automatic-measure-numbers", ":by-line"),)

		self.overall_conductor_attributes = tuple()
		self.overall_conductor_attributes += (("title-separation", "3"),)
		self.overall_conductor_attributes += (("output-file", "\""+self.path+"\""),)
		self.overall_conductor_attributes += (("page-height", 8.3),)
		self.overall_conductor_attributes += (("page-width", 11.7),)
		self.overall_conductor_attributes += (("size", 12),)
		self.overall_conductor_attributes += (("automatic-page-numbers", "t"),)
		self.overall_conductor_attributes += (("first-measure-number", 1),) 
		self.overall_conductor_attributes += (("automatic-measure-numbers", ":by-line"),)

		self.specialStaff = {"trombone":"bass", "sousaphone":"bass", "drums":"percussion"}
		self.transpositions = {"trumpet":2, "sousaphone":14, "tenorsax":14, "altosax":9}
		self.dynamics = ["ppp", "pp", "p", "mp", "mf", "f", "ff", "fff"]

	def setAllParts(self, meta, bars, structure):
		self.meta = meta
		self.names = []
		self.instruments = []
		self.systems = {}

		if len(structure["output"]) == len(bars):
			rehearsalMarks = []
		else:
			rehearsalMarks = [0]
			for section in structure["output"]:
				rehearsalMarks.append( self.getNumBars(structure, section) )
			rehearsalMarks.pop()

		for instrument in meta["instruments"]:
			if instrument in self.instruments:
				continue
			voicesNames = [meta["names"][i] for i in range(meta["n"]) if meta["instruments"][i]==instrument]
			self.names += voicesNames

			time_signature = meta["time_signature"]
			if instrument == "drums":
				key = self.getCmnKey("C")
			else:
				key = self.getCmnKey(meta["key"])
			self.makeNoteNames(self.getTransposedKey(key, instrument))

			self.systems[instrument] = ("staff",
				("staff-name", "\""+instrument+"\""),
				self.getStaff(instrument),
				("meter", time_signature[0], time_signature[1]),
				self.getTransposedKey(key, instrument))

			self.dynamic = ''
			tempo = 0
			offset = 0
			for b in range(len(bars)):
				bar = bars[b]
				self.systems[instrument] = self.systems[instrument] + ("bar",)

				if bar["tempo"] != tempo:
					tempo = bar["tempo"]
					self.systems[instrument] = self.systems[instrument] + (("mm", tempo),)

				if b in rehearsalMarks:
					self.systems[instrument] = self.systems[instrument] + ("interior-double-bar",)
					self.systems[instrument] = self.systems[instrument] + (("rehearsal-letter",),)

				if bar['ts'] != time_signature:
					time_signature = bar['ts']
					self.systems[instrument] = self.systems[instrument] + (("meter", time_signature[0], time_signature[1]),)

				if instrument != "drums" and self.getCmnKey(bar["key"]) != key:
					oldKey = key
					key = self.getCmnKey(bar["key"])
					self.makeNoteNames(self.getTransposedKey(key, instrument))
					if self.getTransposedKey(oldKey, instrument)!="c-major":
						self.systems[instrument] = self.systems[instrument] + (("cancel", self.getTransposedKey(oldKey, instrument)),)
					self.systems[instrument] = self.systems[instrument] + (self.getTransposedKey(key, instrument),)

				notes = []
				for voice in voicesNames:
					if voice in bar.keys():
						notes.append(self.makeNotes(bar[voice]["pitch"], bar[voice]["length"], bar[voice]["ann"], instrument))
				notes = self.unifyVoicings(notes, res=[])
				for n in notes:
					n["onset"]+=offset
				notes.sort(key=lambda x:x["onset"])

				if instrument == "drums":
					self.unclutter(notes, max=3)

				if notes == []:
					self.systems[instrument] += ("whole-measure-rest",)
				else:
					self.removeUselessRests(notes)
					notes = tuple((n["note"],)+tuple((k,)+n[k] if isinstance(n[k], tuple) else (k,n[k]) for k in n.keys() if k not in ["note","flags"])+n["flags"] for n in notes)
					self.systems[instrument] += notes

				offset += time_signature[0]*time_signature[1]/4
			self.systems[instrument] += ("double-bar",)

			if instrument not in self.instruments:
				self.instruments.append(instrument)







	def removeUselessRests(self, notes):
		uselessRests = []
		for note in notes[:-1]:
			if note["note"]=="rest":
				uselessRests.append(note)
		for rest in uselessRests:
			notes.remove(rest)

	def unifyVoicings(self, allNotes, res=[]):
		if len(allNotes)==0:
			return res
		elif len(allNotes)==1:
			return res + allNotes[0]
		elif sum([i==[] for i in allNotes]) > 0:
			return res + sum(allNotes, [])
		else:
			chord = tuple()
			rq = allNotes[0][0]["rq"]
			onset = allNotes[0][0]["onset"]
			flags = tuple()
			for n in allNotes:
				if n[0]["rq"] != rq:
					return res + sum(allNotes, [])
				elif n[0]["onset"] != onset:
					return res + sum(allNotes, [])
				if len(chord)==0 or n[0]["note"]!="rest":
					note = (n[0]["note"],)
					if "note-head" in n[0].keys():
						note += (("note-head", n[0]["note-head"]),)
						note = (note,)
					chord += note
				flags += n[0]["flags"]
			if chord == ("rest",):
				res.append({"note":"rest", "rq":rq, "onset":onset, "flags":tuple()})
			else:
				res.append({"note":"chord", "notes":chord, "rq":rq, "onset":onset, "flags":tuple(set(flags))})
			return self.unifyVoicings([i[1:] for i in allNotes], res)


	def makeNotes(self, pitch, length, dynamics, instrument):
		notes = []
		offset = 0
		for p,l,d in zip(pitch, length, dynamics):
			if p==-1:
				if len(notes)>0 and notes[-1]["rq"]==l:
					offset -= l ; l *= 2
					note = notes[-1]
					note["rq"] = l
					note["flags"] = note["flags"]+("staccato",) if "flags" in note.keys() else ("staccato",)
					notes = notes[:-1]
				else:
					note = {"note":"rest", "rq": l}
			elif instrument=="drums":
				if p in Printer.percNotes.keys():
					note = {"note":Printer.percNotes[p][0], "rq": l, "note-head": Printer.percNotes[p][1]}
				else:
					note = {"note":"d4", "rq": l, "note-head": ":triangle"}
			else:
				note = {"note": self.noteNames[p + self.transpose(instrument)], "rq":l}
				for dd in d:
					if dd not in self.dynamics:
						continue
					elif dd != self.dynamic:
						self.dynamic = dd
						note["flags"] = (self.dynamic,)
			note["onset"] = offset
			if "flags" not in note.keys():
				note["flags"] = tuple()
			notes.append(note)
			offset += l
		notes.sort(key=lambda x: x["onset"])
		return notes

	def makeConductor(self):
		lisp = cl4py.Lisp()
		load = lisp.function("load")
		load("src/cmn/cmn-all.lisp")
		lisp.eval( ("in-package", ":cmn") )
		lisp.eval( 
			("cmn", 
			("title", "\""+self.meta["title"]+"\""),
			*self.overall_conductor_attributes, 
			("system", "bracket", *tuple(
				self.systems[self.instruments[n]] if n==0 else
				self.removeUselessMarks(self.systems[self.instruments[n]]) for n in range(len(self.instruments))))) )

		if not self.makePdf(self.path, conductor=True):
			return

		n=1
		filesToJoin = [self.path+".pdf"]
		while os.path.isfile(self.path+"-"+str(n)+".eps"):
			self.makePdf(self.path+"-"+str(n), conductor=True)
			filesToJoin.append(self.path+"-"+str(n)+".pdf")
			n+=1
		self.gsCommand("src/joinPdfs.sh", *tuple(filesToJoin))

	def makePart(self, instrument):
		path = self.path+"-"+instrument
		lisp = cl4py.Lisp()
		load = lisp.function("load")
		load("src/cmn/cmn-all.lisp")
		lisp.eval( ("in-package", ":cmn") )
		lisp.eval(
			("cmn",
			("title", "\""+self.meta["title"]+" "+instrument+"\""),
			("output-file", "\""+path+"\""),
			*self.overall_part_attributes,
			self.systems[instrument]) )
		self.makePdf(path)

	def makeParts(self):
		conductorProcess = mp.Process(target=Printer.makeConductor, args=(self,))
		conductorProcess.start()
		instrumentProcesses = []
		for instrument in self.instruments:
			# self.printTuple(self.systems[instrument])
			p = mp.Process(target=Printer.makePart, args=(self,instrument))
			p.start()
			instrumentProcesses.append(p)
		conductorProcess.join()
		[p.join() for p in instrumentProcesses]

	def makePdf(self, file, conductor=False):
		if conductor:
			script = "src/makeConductorPdf.sh"
		else:
			script = "src/makePartPdf.sh"
		try:
			self.gsCommand(script, file+".eps", file+".pdf")
			os.remove(file+".eps")
		except subprocess.CalledProcessError:
			return False
		return True

	def gsCommand(self, script, *args):
		try:
			FNULL = open(os.devnull, 'w')
			subprocess.run([script] + list(args), stdout=FNULL, stderr=subprocess.STDOUT)
		except subprocess.CalledProcessError:
			print("ERROR: gs needs to be installed to output pdf files")
			raise


	def getStaff(self, track):
		if track in self.specialStaff.keys():
			return self.specialStaff[track]
		else:
			return "treble"

	def transpose(self, track):
		if track in self.transpositions.keys():
			return self.transpositions[track]
		else:
			return 0

	def getTransposedKey(self, key, track):
		def tonaGen():
			tonas = ["c-major", "df-major","d-major","ef-major","e-major","f-major","gf-major","g-major","af-major","a-major","bf-major","b-major"]
			while True:
				for t in tonas:
					yield t
		tona = tonaGen()
		res = next(tona)
		while res!=key:
			res = next(tona)
		for _ in range((self.transpose(track))%12):
			res = next(tona)
		return res

	def makeNoteNames(self, key):
		self.noteNames = []
		def noteGen():
			scales = {
				'c-major':  ['c','df','d','ef','e','f','gf','g','af','a','bf','b'],
				'f-major':  ['c','df','d','ef','e','f','gf','g','af','a','b','bn'],
				'bf-major': ['c','df','d','e','en','f','gf','g','af','a','b','bn'],
				'ef-major': ['c','df','d','e','en','f','gf','g','a','an','b','bn'],
				'af-major': ['c','d','dn','e','en','f','gf','g','a','an','b','bn'],
				'df-major': ['c','d','dn','e','en','f','g','gn','a','an','b','bn'],
				'gf-major': ['cn','d','dn','e','en','f','g','gn','a','an','cf','bn'],
				'g-major':  ['c','cs','d','ds','e','fn','f','g','gs','a','as','b'],
				'd-major':  ['cn','c','d','ds','e','fn','f','g','gs','a','as','b'],
				'a-major':  ['cn','c','d','ds','e','fn','f','gn','g','a','as','b'],
				'e-major':  ['cn','c','dn','d','e','fn','f','gn','g','a','as','b'],
				'b-major':  ['cn','c','dn','d','e','fn','f','gn','g','an','a','b'],
			}
			while True:
				for n in scales[key]:
					yield n
		note = noteGen()
		oct=-2
		for n in range(128):
			if n%12==0:
				oct+=1
			self.noteNames.append(next(note)+str(oct))

	def getCmnKey(self, key):
		cmnKeys = {
			'C': 'c-major',
			'Am': 'c-major',
			'G': 'g-major',
			'Em': 'g-major',
			'D': 'd-major',
			'Bm': 'd-major',
			'A': 'a-major',
			'F#m': 'a-major',
			'E': 'e-major',
			'C#m': 'e-major',
			'B': 'b-major',
			'G#m': 'b-major',
			'F#': 'gf-major',
			'D#m': 'gf-major',
			'C#': 'df-major',
			'A#m': 'df-major',
			'F': 'f-major',
			'Dm': 'f-major',
			'Bb': 'bf-major',
			'Gm': 'bf-major',
			'Eb': 'ef-major',
			'Cm': 'ef-major',
			'Ab': 'af-major',
			'Fm': 'af-major',
			'Db': 'df-major',
			'Bbm': 'df-major',
			'Gb': 'gf-major',
			'Ebm': 'gf-major'
		}
		return cmnKeys[key]

	percNotes = {
		44: ["d4", ":x"], # hi hat
		42: ["g5", ":x"],
		46: ["g5", ":circled-x"],
		54: ["g5", ":x"], # tambourine
		82: ["g5", ":triangle"], #shaker
		35: ["f4", ":square"], # bass drums
		36: ["f4", ":square"],
		41: ["g4", ":normal"], # toms
		43: ["g4", ":normal"],
		45: ["a4", ":normal"],
		47: ["b4", ":normal"],
		48: ["d5", ":normal"],
		50: ["e5", ":normal"],
		38: ["c5", ":normal"], # snares
		37: ["c5", ":x"],
		51: ["f5", ":x"], # ride
		59: ["f5", ":x"],
		53: ["f5", ":diamond"],
		49: ["a5", ":x"], # crash
		55: ["a5", ":x"],
		57: ["a5", ":x"],
		52: ["a5", ":diamond"],
		56: ["e5", ":triangle"], # cowbell
		32: ["f5", ":triangle"], # metronomes
		34: ["f5", ":triangle"]
	}

	def removeUselessMarks(self, system):
		return tuple([i if not isinstance(i, tuple) else self.removeUselessMarks(i) for i in system if 
			(i != "rehearsal-letter" and 
			(not isinstance(i, tuple) or len(i)==0 or i[0] != "mm"))])

	def getNumBars(self, structure, part=None):
		if part == None:
			return self.getNumBars(structure, structure["output"])
		if isinstance(part, list):
			return sum([self.getNumBars(structure, p) for p in part])
		elif part in structure.keys():
			return self.getNumBars(structure, structure[part])
		elif part in structure['BARS']:
			return 1
		else:
			raise ValueError("unvalid part :"+part)

	def printTuple(self, inTuple, indent=""):
		'''for debug, print tuple as tree'''
		for i in inTuple:
			if isinstance(i, tuple):
				self.printTuple(i, indent=indent+"  ")
			else:
				print(indent + str(i))

	def unclutter(self, notes, max=3):
		if notes==[]:
			return
		o = notes[0]["onset"]
		s = 0
		notesToRemove = []
		for n in notes:
			if n["onset"]==o:
				s+=1
				if s>max:
					notesToRemove.append(n)
			else:
				s=1
				o=n["onset"]
		for n in notesToRemove:
			notes.remove(n)