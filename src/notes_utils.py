from src.constants import *


def isnote(note):
    if note not in NOTES:
        print(note + " is not a note!")
        exit()

def iskey(key):
    if key not in KEYS:
        print(key + " is not a key!")
        exit()

def ispitch(pitch):
    if pitch < 0 or pitch >= N_NOTES:
        print("Pitch " + str(pitch) + " is out of range [0 -> 127]")
        exit()

def isoctave(octave):
    if octave not in OCTAVES:
        print("Octave " + str(octave) + " is out of range [-1 -> 9]")
        exit()

def issharp(item, key=False):
    if key:
        iskey(item)
        return True if item in SHARP_KEYS else False
    else:
        isnote(item)
        return True if item in SHARP_NOTES else False

def istempo(tempo):
    if tempo > 300 or tempo < 0:
        print("Tempo is out of range [0 -> 300]")
        exit()

def isminor(key):
    iskey(key)
    return True if MINOR_ in key else False

def sharp2flat(note):
    isnote(note)
    return chr(ord(note[0]) + 1).replace("H", "A") + FLAT if SHARP in note else note

def flat2sharp(note):
    isnote(note)
    return chr(ord(note[0]) - 1).replace("@", "G") + SHARP if FLAT in note else note

def key2note(key):
    iskey(key)
    return key.replace(MINOR_, "")

def note2pitch(note, octave):
    isnote(note)
    isoctave(octave)
    pitches = SHARP_PITCHES if issharp(note) else FLAT_PITCHES
    try:
        return pitches[note][OCTAVES.index(octave)]
    except:
        print("Note " + note + str(octave) + " is out of range [C-1 -> G9]")
        exit()

def pitch2note(pitch, key):
    ispitch(pitch)
    iskey(key)
    notes = SHARP_NOTES if issharp(key, key=True) else FLAT_NOTES
    for note in notes:
        if pitch in PITCHES[note]:
            return note, OCTAVES[PITCHES[note].index(pitch)]

def major2minor(major_key):
    iskey(major_key)
    if isminor(major_key):
        return major_key
    minor_pitch = note2pitch(key2note(major_key), 4) - 3
    minor_key, _ = pitch2note(minor_pitch, major_key)
    return minor_key + MINOR_

def minor2major(minor_key):
    iskey(minor_key)
    if not isminor(minor_key):
        return minor_key
    major_pitch = note2pitch(key2note(minor_key), 4) + 3
    major_key, _ = pitch2note(major_pitch, minor_key)
    return major_key

def get_sharps_flats(key):
    key = minor2major(key)
    return SHARP_KEYS.index(key) if issharp(key, key=True)\
        else - (FLAT_KEYS.index(key) + 1)

