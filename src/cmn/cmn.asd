;;; ASDF system definition file for CMN
;;; For information on ASDF see: http://www.cliki.net/asdf
;;;
;;; To load CMN from a non-standard install location:
;;;
;;; (require :asdf)
;;; (push "/path/to/cmn/" asdf:*central-registry*)
;;; (asdf:operate 'asdf:load-source-op :cmn)
;;;
;;; To download/install/load CMN from its archive:
;;;
;;; (require :asdf)
;;; (progn (push "/path/to/asdf-install/" asdf:*central-registry*)
;;;        (asdf:operate 'asdf:load-op 'asdf-install))
;;; (asdf-install:install 'cmn)
;;; (asdf:operate 'asdf:load-source-op 'cmn)

(defsystem "cmn"
  :description "Common Music Notation"
  :author "William Schottstaedt <bil (at) ccmra (dot) stanford (dot) edu>"
  :licence "LLGPL"
  :components ((:file "cmn-all" )))

