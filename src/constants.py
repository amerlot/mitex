# STRINGS #

SHARP = "#"
FLAT = "b"
MINOR_ = "m"
METADATA = "METADATA"
BARS = "BARS"
STRUCTURE = "STRUCTURE"
NEW_BAR = "bar"
BAR_UPDATE_SYMBOL = "!"
BAR_UPDATES = ["time_signature", "key", "tempo"]
TITLE_MTX = "title"
AUTHOR = "author"
COMMENT = "%"
SEP = ","
INTRA_SEP = "_"
TS_SEP = "/"
RC = "\n"
EQUAL = "="
REUSE = "@"
MANDATORY_META_ITEMS = ["tempo", "key", "time_signature",\
    "instruments", "names", "volume", "title", "author"]
INT_META_ITEMS = ["tempo", "volume"]
LIST_META_ITEMS = ["instruments", "names"]
OCTAVE_UP = "+"
OCTAVE_DOWN = "-"
OUTPUT = "output"
EXTENSION = ".mtx"
REST_PITCH = -1
NOTES_LENGTH = {
    "w" :   4,
    "h" :   2,
    "q" :   1,
    "e" :   0.5,
    "s" :   0.25,
    "t" :   0.125,
}
DOT = "."
DOT_FACTOR = 1.5
ACCENT = ">"
TIED = "~"
ANNOTATIONS = {
    "ppp"   :   0.2,
    "pp"    :   0.4,
    "p"     :   0.6,
    "mp"    :   0.8,
    "mf"    :   1,
    "f"     :   1.25,
    "ff"    :   1.5,
    "fff"   :   2,
    ACCENT  :   10,
    "st"    :   0.75,
    # TODO
    # "le"    :   1.25,
    TIED    :   0, # tied note
}
LENGTH_ANNOTATIONS = ["st"] #, "le"]
DYNAMICS = ["ppp", "pp", "p", "mp", "mf", "f", "ff", "fff"]
REVERSED_LENGTH = dict(map(reversed, NOTES_LENGTH.items()))
for l in list(REVERSED_LENGTH.keys()):
    new_length = l * DOT_FACTOR
    REVERSED_LENGTH[new_length] = REVERSED_LENGTH[l] + DOT
# for l in list(REVERSED_LENGTH.keys()): # TODO
#     new_length = l * ANNOTATIONS["st"]
#     REVERSED_LENGTH[new_length] = REVERSED_LENGTH[l] + "_st"
LENGTHS = list(REVERSED_LENGTH.keys())
REVERSED_ANNS = dict(map(reversed, ANNOTATIONS.items()))
DRUM_SHOTS = {
    "0" :   36, # bass drum
    "1" :   38, # snare drum
    "2" :   42, # closed hihat
    "3" :   46, # open hihat
    "4" :   49, # crash
    "5" :   55, # splash
    "6" :   59, # ride 2
    "7" :   76, # high woodblock
    "8" :   77, # low woodblock
    "9" :   52, # cymbal
    "10":   65, # high timbale
    "11":   66, # low timbale
    "12":   51, # ride 1
    "13":   35, # bass drum 2
    "14":   50, # high tom
    "15":   43, # high floor tom
    "16":   48, # high mid tom
    "17":   47, # low mid tom
    "18":   41, # low floor tom
    "19":   45, # low tom
    "20":   37, # side stick
    "21":   57, # crash 2
    "22":   39, # hand clap
    "23":   63, # open high conga
    "24":   64, # low conga
    "25":   82, # shekere
    "26":   56, # cowbell
}
REVERSED_DRUMS = dict(map(reversed, DRUM_SHOTS.items()))
LENGTH_RESOLUTION = 3
MAX_ITER = 900

# PY_MIDICSV

BPM_TO_TEMPO = 60000000
TICKS = 960
START = "Start_track"
END = "End_track"
HEADER = "Header"
EOF = "End_of_file"
COPYRIGHT = "Copyright_t"
NOTEON = "Note_on_c"
NOTEOFF = "Note_off_c"
ON = 1
OFF = 0
PROGRAM = "Program_c"
CONTROL = "Control_c"
TEMPO = "Tempo"
KEY_SIGNATURE = "Key_signature"
TIME_SIGNATURE = "Time_signature"
MAJOR = "\"major\""
MINOR = "\"minor\""
TITLE = "Title_t"
TEXT = "Text_t"
INSTRUMENTS_NAMES_REPLACE_MAP = {
    "\""                :   "",
    "[0-9]"             :   "",
    "drumset"           :   "drums",
    "saxophone"         :   "sax",
    "inbb"              :   "",
    "ineb"              :   "",
    "timbales"          :   "drums",
    "cl"                :   "drums",
    "woodblock"         :   "drums",
    "alto"              :   "altosinger",
    "altosingersax"     :   "altosax",
    "snaredrum"         :   "drums",
    "shekere"           :   "drums",
    "congas"            :   "drums",
    "tenorsolo"         :   "altosinger",
    "marchingbassdrum"  :   "drums",
    "dumbek"            :   "drums",
    "mambobell"         :   "drums",
    "drumss"            :   "drums",
}
# MIDI STANDARD #

N_NOTES = 128
DYNNAMICS_DOWNSAMPLE = N_NOTES / len(DYNAMICS)
OCTAVES = [i for i in range(-1, 10)]

# MUSIC #

N_NOTES_IN_OCTAVE = 12
PITCHES = {
    "C"     :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  0],
    "C#"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  1],
    "Db"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  1],
    "D"     :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  2],
    "D#"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  3],
    "Eb"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  3],
    "E"     :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  4],
    "F"     :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  5],
    "F#"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  6],
    "Gb"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  6],
    "G"     :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  7],
    "G#"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  8],
    "Ab"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  8],
    "A"     :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE ==  9],
    "A#"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE == 10],
    "Bb"    :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE == 10],
    "B"     :   [i for i in range(N_NOTES) if i % N_NOTES_IN_OCTAVE == 11],
}
NOTES = list(PITCHES.keys())
SHARP_NOTES = [note for note in NOTES if FLAT not in note]
FLAT_NOTES = [note for note in NOTES if SHARP not in note]
SHARP_PITCHES = {note: PITCHES[note] for note in SHARP_NOTES}
FLAT_PITCHES = {note: PITCHES[note] for note in FLAT_NOTES}
SHARP_KEYS = ["C",  "G",  "D",  "A",   "E",   "B",   "F#",  "C#",\
              "Am", "Em", "Bm", "F#m", "C#m", "G#m", "D#m", "A#m"]
FLAT_KEYS = ["F",  "Bb", "Eb", "Ab", "Db",  "Gb",\
             "Dm", "Gm", "Cm", "Fm", "Bbm", "Ebm"]
KEYS = SHARP_KEYS + FLAT_KEYS
MAJOR_SHIFTS =[0, 2, 4, 5, 7, 9, 11]
MINOR_SHIFTS =[0, 2, 3, 5, 7, 8, 10]

# INSTRUMENTS #

"""
program values to be found here:
https://www.midi.org/specifications/item/gm-level-1-sound-set
"""

INSTRUMENTS = {
    "piano"         :   {
        "name"      :   "Piano",
        "shortname" :   "pi",
        "highnote"  :   "C4",
        "program"   :   1,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  67],
            [ 10,  74],
            [ 7,  120],
        ],
    },
    "trumpet"       :   {
        "name"      :   "Trumpet",
        "shortname" :   "tp",
        "highnote"  :   "Bb4",
        "program"   :   57,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  62],
            [ 10,  81],
            [ 7,  106],
        ],
    },
    "trombone"      :   {
        "name"      :   "Trombone",
        "shortname" :   "tb",
        "highnote"  :   "Bb3",
        "program"   :   58,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  67],
            [ 10,  74],
            [ 7,  120],
        ],
    },
    "altosax"       :   {
        "name"      :   "Alto Saxophone",
        "shortname" :   "al",
        "highnote"  :   "F4",
        "program"   :   66,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  48],
            [ 10,  51],
            [ 7,   95],
        ],
    },
    "tenorsax"      :   {
        'name'      :   "Tenor Saxophone",
        "shortname" :   "te",
        "highnote"  :   "Bb3",
        "program"   :   67,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  48],
            [ 10,  76],
            [ 7,   95],
        ],
    },
    "sousaphone"    :   {
        "name"      :   "Sousaphone",
        "shortname" :   "so",
        "highnote"  :   "Bb2",
        "program"   :   59,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  57],
            [ 10,  64],
            [ 7,  116],
        ],
    },
    "altosinger"    :   {
        "name"      :   "Alto Singer",
        "shortname" :   "sg",
        "highnote"  :   "Bb3",
        "program"   :   53,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  48],
            [ 10,  41],
            [ 7,  126],
        ],
    },
    "drums"       :   {
        "name"      :   "Drum Set",
        "shortname" :   "dr",
        "highnote"  :   "C4",
        "program"   :   1,
        "controls"  :   [
            [121,   0],
            [ 64,   0],
            [ 91,  19],
            [ 10,  64],
            [ 7,  114],
        ],
    },
}

