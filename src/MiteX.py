import os
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = ""
import pygame
import py_midicsv as pm
from src.print import Printer
from src.notes_utils import *
import numpy as np
import pandas as pd
import re


class MiteX:

    def __init__(self, path, play, score, csv, reverse):

        if reverse:
            TexmI(path, csv)
        else:
            self.parse_mtx(path)
            self.mtx_to_csv(csv=csv)
            if score:
                self.printer = Printer(path.replace(EXTENSION, ""))
                self.printer.setAllParts(self.meta, self.output_bars, self.struct)
                self.printer.makeParts()
            self.csv_to_mid(play=play)

    def error(self, msg):

        print(RC + "/!\\ DEBUG /!\\ " + msg + RC)
        exit()

    def parse_mtx(self, path):

        if not path.endswith(EXTENSION):
            self.error("The input file should have the .mtx extension!")
        self.get_mtx(path)
        self.get_meta()
        self.get_struct()
        self.get_bars()

    def get_mtx(self, path):

        try:
            with open(path, "r") as f:
                lines = f.read()
                self.title_author = [l for l in lines.split(RC)\
                    if l.startswith(AUTHOR) or l.startswith(TITLE_MTX)]
                lines = lines.replace(" ", "").split(RC)
        except:
            self.error("Cannot read file " + path)
        self.csv_path = path.replace(EXTENSION, ".csv")
        self.mid_path = path.replace(EXTENSION, ".mid")
        lines = [l for l in [l.split(COMMENT)[0] for l in lines] if len(l) > 0]
        self.mtx = RC.join(lines)

    def is_list_uniques(self, l):

        return len(l) == len(set(l))

    def get_meta(self):

        # CHECK IF ALL SECTIONS ARE PRESENT #
        previous_loc = 0
        for item in [METADATA, BARS, STRUCTURE]:
            if item not in self.mtx:
                self.error("`" + item + "` is absent :(")
            if self.mtx.count(item) != 1:
                self.error("`" + item + "` should be written only once!")
            item_loc = len(self.mtx.split(item)[0])
            if item_loc < previous_loc:
                self.error("Sections must be given in the following order: " +\
                    "METADATA, BARS, STRUCTURE")
            previous_loc = item_loc

        # GET METADATA #
        meta = self.mtx.split(METADATA)[-1].split(BARS)[0]
        meta = [l for l in meta.split(RC) if len(l) > 0]
        if not all([m.count(EQUAL) == 1 for m in meta]):
            self.error("All metadata must be set using the format `item = value`" +\
                ". There must be one and only one `=` symbol in each line.")
        self.meta = dict([l.split(EQUAL) for l in meta])
        self.title_author = dict([l.split(EQUAL) for l in self.title_author])
        for k in list(self.title_author.keys()):
            self.title_author[k.replace(" ", "")] = self.title_author.pop(k)
        for item in MANDATORY_META_ITEMS:
            if item not in self.meta.keys():
                self.error("Metadata: you must set the `" + item + "` item!")
            if item == "key":
                iskey(self.meta[item])
        self.meta["title"] = self.title_author["title"].lstrip(" ").rstrip(" ")
        self.meta["author"] = self.title_author["author"].lstrip(" ").rstrip(" ")
        for item in INT_META_ITEMS:
            if item in self.meta.keys():
                if not self.meta[item].isdigit():
                    self.error("Metadata: item `" + item + "` should be " +\
                        "a positive integer...")
                self.meta[item] = int(self.meta[item])
        istempo(self.meta["tempo"])
        for item in LIST_META_ITEMS:
            if item in self.meta.keys():
                self.meta[item] = self.meta[item].split(SEP)
        ts = self.meta["time_signature"].split(TS_SEP)
        if len(ts) != 2 or not all([x.isdigit() for x in ts]):
            self.error("Metadata: time signature should be set using the " +\
                "format `m/n`, m and n being positive integers")
        self.meta["time_signature"] = [int(x) for x in ts]
        if len(self.meta["instruments"]) != len(self.meta["names"]):
            self.error("Metadata: instruments and names lists must " +\
                "have the same length!")
        if not self.is_list_uniques(self.meta["names"]):
            self.error("Metadata: each instrument must have its own name!")
        for name in self.meta["names"]:
            if name in ["ts", "key", "tempo"]:
                self.error("Metadata: instrument shall not be named `ts`, `key`" +\
                    " or `tempo`")
        for instrument in self.meta["instruments"]:
            if instrument not in INSTRUMENTS:
                instruments_list = ", ".join(list(INSTRUMENTS.keys()))
                self.error("Metadata: instrument " + instrument + " is not " +\
                    "defined! You must pick in the list: " + instruments_list)
        self.meta["n"] = len(self.meta["instruments"])

    def get_bars(self):

        time = 0
        last_ts, last_key, last_tempo = self.meta["time_signature"],\
            self.meta["key"], self.meta["tempo"]
        self.bar_updates = [[time, last_ts, "ts"], [time, last_key, "key"],\
            [time, last_tempo, "tempo"]]
        self.output_bars = []

        for bar_name in self.bar_list:
            bar = self.checked_bars[bar_name]
            update = bar[BAR_UPDATE_SYMBOL]
            update_types = list(update.keys())
            if "time_signature" in update_types:
                ts = update["time_signature"].split(TS_SEP)
                if len(ts) != 2 or not all([x.isdigit() for x in ts]):
                    self.error("In `" + bar_name + "`: time signature should " +\
                        "be set using the format `m/n`, m and n being " +\
                        "positive integers")
                last_ts = [int(x) for x in ts]
                self.bar_updates.append([time, last_ts, "ts"])
            if "key" in update_types:
                last_key = update["key"]
                iskey(last_key)
                self.bar_updates.append([time, last_key, "key"])
            if "tempo" in update_types:
                if not update["tempo"].isdigit():
                    self.error("In `" + bar_name + "`: tempo should be " +\
                        "a positive integer")
                last_tempo = int(update["tempo"])
                istempo(last_tempo)
                self.bar_updates.append([time, last_tempo, "tempo"])
            output_bar = {"ts": last_ts, "key": last_key, "tempo": last_tempo}
            n_beats_in_bar = output_bar["ts"][0]
            time += round(n_beats_in_bar * TICKS)
            for name in [name for name in bar.keys() if name != BAR_UPDATE_SYMBOL]:
                notes = [note for note in bar[name].split(SEP) if len(note) > 0]
                output_bar[name] = {"pitch": [], "length": [], "ann": [],\
                    "tied": False}
                instrument = self.meta["instruments"][self.meta["names"].index(name)]
                highnote = INSTRUMENTS[instrument]["highnote"]
                if not highnote[-1].isdigit():
                    self.error(instrument.upper() + "'s highnote should use the " +\
                        "format `[note][octave]` where octave is a postive integer")
                highpitch = note2pitch(highnote[:-1], int(highnote[-1]))
                shifts = MINOR_SHIFTS if isminor(last_key) else MAJOR_SHIFTS
                default_pitch = max([p for p in PITCHES[key2note(last_key)]\
                    if p <= highpitch])
                for note in notes:
                    pitch, length, ann = self.parse_note(note, shifts, default_pitch,\
                        bar_name, name, instrument=="drums")
                    output_bar[name]["pitch"].append(pitch)
                    output_bar[name]["length"].append(length)
                    output_bar[name]["ann"].append(ann)

                actual_sum = sum(output_bar[name]["length"])
                if actual_sum > n_beats_in_bar:
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "` is too long!")
                elif actual_sum + (1/(10 ** (LENGTH_RESOLUTION - 1))) < n_beats_in_bar:
                    delta = n_beats_in_bar - actual_sum
                    output_bar[name]["pitch"].append(REST_PITCH)
                    output_bar[name]["length"].append(delta)
                    output_bar[name]["ann"].append([])
            self.output_bars.append(output_bar)

    def parse_note(self, note, shifts, default_pitch, bar_name, name, isdrum):

        note_dbg = note
        if note.count(INTRA_SEP) < 1:
            self.error("Line `" + name + "` of `" + bar_name +\
                "`: there is no `_` in note `" + note_dbg + "`")
        note = re.sub(INTRA_SEP + INTRA_SEP + "*", INTRA_SEP, note)
        if note == INTRA_SEP:
            self.error("Line `" + name + "` of `" + bar_name +\
                "`: note `" + note_dbg + "` is not well formatted...")
        note_split = [n for n in note.split(INTRA_SEP)]
        while len(note_split[-1]) == 0:
            note_split = note_split[:-1]
        if len(note_split) < 2:
            self.error("Line `" + name + "` of `" + bar_name +\
                "`: note `" + note_dbg + "` is not well formatted...")
        degree, length = note_split[0], note_split[1]
        if len(note_split) > 2:
            ann = note_split[2:]
            for a in ann:
                if a not in ANNOTATIONS.keys():
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "`: annotation `" + a + "` does not exist!")
            if len([a for a in DYNAMICS if a in ann]) > 1:
                self.error("Line `" + name + "` of `" + bar_name +\
                        "`: You shall not give more than one dynamic in note `" +\
                        note + "`")
        else:
            ann = []
        if isdrum:
            if degree == "":
                degree = REST_PITCH
            else:
                if not degree.isdigit():
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "`: drum notation should be a positive integer in note `" +\
                        note + "`")
                if degree not in DRUM_SHOTS.keys():
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "`: drum notation is out of range [0 -> " +\
                        max(list(DRUM_SHOTS.keys()))  + "] in note `" +\
                        note + "`")
                degree = DRUM_SHOTS[degree]
        else:
            octave_change = (degree.count(OCTAVE_UP) -\
                degree.count(OCTAVE_DOWN)) * N_NOTES_IN_OCTAVE
            degree = degree.replace(OCTAVE_UP, "").replace(OCTAVE_DOWN, "")
            alteration = (degree.count(SHARP) - degree.count(FLAT))
            degree = degree.replace(SHARP, "").replace(FLAT, "")
            if degree.isdigit():
                degree = int(degree)
                if degree < 1 or degree > 7:
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "`: degree should be between 1 and 7 in note `" + note_dbg + "`")
            elif degree == "":
                degree = REST_PITCH
            else:
                self.error("Line `" + name + "` of `" + bar_name +\
                    "`: degree should be an integer or empty in note `" + note_dbg + "`")
            if degree != REST_PITCH:
                degree = default_pitch + shifts[degree - 1] + octave_change + alteration
                ispitch(degree)
        try:
            if length[0] in NOTES_LENGTH.keys():
                dots = length.count(DOT)
                if dots > 1:
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "`: maximum one dot in note `" + note_dbg + "`")
                length = NOTES_LENGTH[length[0]]
                if dots:
                    length *= DOT_FACTOR
            else:
                length = float(length)
        except:
            self.error("Line `" + name + "` of `" + bar_name +\
                "`: length should be a real number or a symbol (q for a quarter " + \
                "note, e for an eighth note, etc.) in note `" + note_dbg + "`")

        return degree, length, ann

    def get_struct(self):

        self.bars = {}
        bars = self.mtx.split(BARS)[-1].split(STRUCTURE)[0]
        bars = [bar for bar in bars.split(RC + NEW_BAR) if bar != RC and len(bar) > 0]
        for bar in bars:
            bar = [l for l in bar.split(RC) if len(l) > 0]
            bar_name = NEW_BAR + bar[0]
            self.bars[bar_name] = bar

        struct = self.mtx.split(STRUCTURE)[-1]
        struct = [s for s in struct.split(RC) if len(s) > 0]
        if not all([s.count(EQUAL) == 1 for s in struct]):
            self.error("All structure data must be set using the format " +\
                "`item = value1, value2...` and there must be " +\
                "one and only one `=` symbol in each line.")
        struct = dict([s.split(EQUAL) for s in struct])
        if OUTPUT not in struct.keys():
            self.error("You must give an `output` item!")
        bar_and_part = [b for b in list(self.bars.keys()) if b in list(struct.keys())] +\
            [p for p in list(struct.keys()) if p in list(self.bars.keys())]
        if len(bar_and_part) > 0:
            self.error("`" + bar_and_part[0] + "` is both a part of the structure " +\
                "and a bar!")
        for k in struct.keys():
            struct[k] = [part for part in struct[k].split(SEP) if len(part) > 0]
        struct[BARS] = list(self.bars.keys())
        self.struct = struct
        self.bar_list = []
        self.n_iter = 0
        self.check_struct(OUTPUT)
        self.checked_bars = {}
        for bar_name in self.bar_list:
            self.n_iter = 0
            self.handle_bar_references(bar_name)

    def check_struct(self, key):

        if self.n_iter > MAX_ITER:
            self.error("There seems to be a loop involving `" + key + "`")
        for item in self.struct[key]:
            if item not in self.struct.keys() and item not in self.struct[BARS]:
                self.error("There is no part or bar called `" + item + "`")
            if item in self.struct.keys():
                self.n_iter += 1
                self.check_struct(item)
            else:
                self.bar_list.append(item)

    def handle_bar_references(self, bar_name):

        if self.n_iter> MAX_ITER:
            self.error("There seems to be a loop involving `" + bar_name + "`")

        if bar_name in self.checked_bars.keys():
            return self.checked_bars[bar_name]

        bar = self.bars[bar_name][1:]

        # NOTES #
        notes = [l for l in bar if not l.startswith(REUSE)\
            and not l.startswith(BAR_UPDATE_SYMBOL)]
        if len(notes) > self.meta["n"]:
            self.error("There are too many instruments playing in " +\
                "`" + bar_name + "`!")
        for idx_l, l in enumerate(notes):
            if not l.count(EQUAL) == 1:
                self.error("In `" + bar_name + "`: all lines should begin " +\
                    "with the format `name = [sequence of notes]`. There must be " +\
                    "one and only one `=` symbol in each line.")
            name = l.split(EQUAL)[0]
            if name not in self.meta["names"]:
                self.error("The instrument name `" + name + "` in `" + bar_name +\
                    "` was not set in the metadata...")
        notes = dict([l.split(EQUAL) for l in notes])
        instruments = list(notes.keys())
        if not self.is_list_uniques(instruments):
            self.error("At least one instrument's notes were set twice in `" +\
                bar_name + "`")
        reference, links = False, {}
        for name in instruments:
            if NEW_BAR in notes[name]:
                reference = True
                ref = [l for l in notes[name].split(SEP) if len(l) > 0]
                if len(ref) > 1:
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "`: the reference to another bar for one instrument" +\
                        " should use the format `name = barXXX`")
                ref = ref[0]
                if not ref in self.struct[BARS]:
                    self.error("In `" + bar_name + "`: bar to reuse `" +\
                        ref + "` has not been defined!")
                links[name] = ref

        # REUSE #
        reuse = [l for l in bar if l.startswith(REUSE)]
        if len(reuse) > 1:
            self.error("In `" + bar_name + "`: you should give only one " +\
                "bar to reuse: `@ bartoreuse`")
        if len(reuse) == 1:
            bar_to_reuse = reuse[0].replace(REUSE, "")
            if bar_to_reuse not in self.struct[BARS]:
                self.error("In `" + bar_name + "`: bar to reuse `" +\
                    bar_to_reuse + "` has not been defined!")
            reuse = True
        else:
            reuse = False

        # UDPATES #
        update = [l for l in bar if l.startswith(BAR_UPDATE_SYMBOL)]
        if len(update) > 1:
            self.error("In `" + bar_name + "`: you should give all bar " +\
                "updates in one line beginning with `!`")
        if len(update) == 1:
            update = [u for u in update[0].replace(BAR_UPDATE_SYMBOL,\
                "").split(SEP) if len(u) > 0]
            if not all([u.count(EQUAL) == 1 for u in update]):
                self.error("All bar update must be set using the format " +\
                    "`item = value` in `" + bar_name + "`. There must be " +\
                    "one and only one `=` symbol in each definition")
            update = dict([u.split(EQUAL) for u in update])
            update_types = list(update.keys())
            if not self.is_list_uniques(update_types):
                self.error("In `" + bar_name + "`: you must only set once " +\
                    "each bar update")
            if any([t not in BAR_UPDATES for t in update_types]):
                wrong_update = [t for t in update_types if t not in BAR_UPDATES][0]
                self.error("In `" + bar_name + "`: `" + wrong_update +\
                    "` is not a valid bar update. The bar updates must be " +\
                    "among \{tempo, key, time_signature\}")
        else:
            update = {}

        # FILL WITH REFERENCES #
        if reuse:
            self.n_iter += 1
            bar_to_reuse = self.handle_bar_references(bar_to_reuse)
            for item in bar_to_reuse[BAR_UPDATE_SYMBOL].keys():
                if item not in update.keys():
                    update[item] = bar_to_reuse[BAR_UPDATE_SYMBOL][item]
            for name in [name for name in list(bar_to_reuse.keys())\
                if name != BAR_UPDATE_SYMBOL]:
                if name not in instruments:
                    instruments.append(name)
                    notes[name] = bar_to_reuse[name]
        if reference:
            for name in links.keys():
                self.n_iter += 1
                ref = self.handle_bar_references(links[name])
                if name not in ref.keys():
                    self.error("Line `" + name + "` of `" + bar_name +\
                        "`: this instrument was not defined in `" +\
                        links[name] + "`")
                notes[name] = ref[name]

        bar = {}
        bar[BAR_UPDATE_SYMBOL] = update
        for name in instruments:
            bar[name] = notes[name]
        self.checked_bars[bar_name] = bar

        return bar

    def csv_append(self, l):
        if type(l) == str:
            self.csv_lines.append(l + RC)
        elif type(l) == list:
            for item in l:
                self.csv_append(item)

    def bpm_to_csvtempo(self, tempo):
        return round(BPM_TO_TEMPO / tempo)

    def mtx_to_csv(self, csv):

        # HEADER TRACK
        n_tracks = self.meta["n"]
        header = "0, 0, Header, 1, " + str(n_tracks + 1) + ", " + str(TICKS)
        self.csv_lines = []
        self.csv_append(header)

        # META TRACK
        meta = ["1, 0, " + START]
        meta.append("1, 0, " + TITLE + ", \"" + self.meta["title"] + "\"")
        meta.append("1, 0, " + TEXT + ", \"" + self.meta["author"] + "\"")
        for time, u, u_type in self.bar_updates:
            if u_type == "ts":
                meta.append("1, " + str(time) + ", " + TIME_SIGNATURE + ", " +\
                    str(u[0]) + ", 2, 24, 8") # TODO for now, only X/4 time signatures
            if u_type =="key":
                key_n = get_sharps_flats(u)
                key_type = MINOR if isminor(u) else MAJOR
                meta.append("1, " + str(time) + ", " + KEY_SIGNATURE + ", " +\
                    str(key_n) + ", " + str(key_type))
            if u_type == "tempo":
                meta.append("1, " + str(time) + ", " + TEMPO + ", " +\
                    str(self.bpm_to_csvtempo(u)))
        meta.append("1, " + str(time + 1) + ", " + END)
        self.csv_append(meta)

        # INSTRUMENTS TRACKS
        titles = {}
        n_altosinger = 16
        for idx_track in range(n_tracks):
            track_str = str(2 + idx_track)
            name = self.meta["names"][idx_track]
            instrument = self.meta["instruments"][idx_track]
            if instrument == "drums":
                channel = 9
            else:
                channel = idx_track
                if channel == 9:
                    channel = n_tracks
                    if channel == 9:
                        channel = 10
                if instrument == "altosinger":
                    channel = n_altosinger
                    n_altosinger += 1
            title = INSTRUMENTS[instrument]["name"]
            program = INSTRUMENTS[instrument]["program"] - 1 # origin = 0
            controls = INSTRUMENTS[instrument]["controls"]

            if title in titles.keys():
                titles[title] += 1
                title += " " + str(titles[title])
            else:
                titles[title] = 1

            time = 0
            track = [track_str + ", " + str(time) + ", " + START]
            track.append(track_str + ", " + str(time) + ", " + PROGRAM +\
                ", " + str(channel) + ", " + str(program))
            for item, value in controls:
                track.append(track_str + ", " + str(time) + ", " + CONTROL +\
                    ", " + str(channel) + ", " + str(item) + ", " + str(value))
            track.append(track_str + ", " + str(time) + ", " + TITLE + ", \"" +\
                title + "\"")

            last_a = {"v": self.meta["volume"], "dynamic": "mf"}
            for idx_bar, bar in enumerate(self.output_bars):
                if name not in bar.keys():
                    pitch, length, ann, tied =\
                        [REST_PITCH], [bar["ts"][0]], [[]], False
                else:
                    pitch, length, ann, tied = bar[name]["pitch"],\
                        bar[name]["length"], bar[name]["ann"], bar[name]["tied"]
                n_notes = len(pitch)
                for idx_note, (p, l, a) in enumerate(zip(pitch, length, ann)):
                    if TIED in a:
                        bar_name = self.bar_list[idx_bar]
                        if idx_note != n_notes - 1:
                            self.error("In `" + bar_name + ": you can only use " +\
                                "tied note on the last note of a bar, and this " +\
                                "note must reach the end of the bar")
                        if p == REST_PITCH:
                            self.error("In `" + bar_name + ": you can only use " +\
                                "tied note on non silent notes!")
                        if idx_bar == len(self.output_bars) - 1:
                            self.error("No tied note on the last bar")
                        bar_name_next = self.bar_list[idx_bar + 1]
                        if name not in self.output_bars[idx_bar + 1]:
                            self.error("In `" + bar_name_next + ": the tied note " +\
                                "that began in `" + bar_name + "` must continue " +\
                                "in `" + bar_name_next + "`")
                        if self.output_bars[idx_bar + 1][name]["pitch"][0] != p:
                            self.error("In `" + bar_name_next + ": the tied note " +\
                                "that began in `" + bar_name + "` must continue " +\
                                "in `" + bar_name_next + "` with the same pitch!")
                        self.output_bars[idx_bar + 1][name]["tied"] = True
                    last_a = self.update_ann(last_a, a, l)
                    if p != REST_PITCH:
                        duration = round(last_a["l"] * TICKS)
                        note_on_str = track_str + ", " + str(time) + ", " + NOTEON +\
                            ", " + str(channel) + ", " + str(p) + ", " + str(last_a["v"])
                        note_off_str = track_str + ", " + str(time + duration) + ", " + NOTEOFF +\
                            ", " + str(channel) + ", " + str(p) + ", 0"
                        if not tied or idx_note > 0:
                            track.append(note_on_str)
                        if not (TIED in a) or idx_note != n_notes - 1:
                            track.append(note_off_str)
                    time += round(l * TICKS)

            track.append(track_str + ", " + str(time + 1) + ", " + END)
            self.csv_append(track)

        # FOOTER TRACK
        footer = "0, 0, End_of_file"
        self.csv_append(footer)

        if csv:
            with open(self.csv_path, "w") as f:
                f.write("".join(self.csv_lines))

    def update_ann(self, last_ann, ann, l):

        meta_v = self.meta["volume"]
        if any([a in ann for a in DYNAMICS]):
            dynamic = [a for a in DYNAMICS if a in ann][0]
        else:
            dynamic = last_ann["dynamic"]
        v = round(ANNOTATIONS[dynamic] * meta_v)
        if ACCENT in ann:
            v += ANNOTATIONS[ACCENT]
        v = 127 if v > 127 else (0 if v < 0 else v)

        # TODO
        if "st" in ann:
            l *= ANNOTATIONS["st"]

        last_ann["v"], last_ann["l"], last_ann["dynamic"] = v, l, dynamic

        return last_ann

    def csv_to_mid(self, play=False):

        midi_object = pm.csv_to_midi(self.csv_lines)
        with open(self.mid_path, "wb") as output_file:
            midi_writer = pm.FileWriter(output_file)
            midi_writer.write(midi_object)
        if play:
            self.play_mid()

    def play_mid(self):

        pygame.init()
        pygame.mixer.music.load(self.mid_path)
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy():
            pygame.time.wait(100)


class TexmI(MiteX):

    def __init__(self, path, csv):

        self.get_csv(path, csv)
        self.csv_to_df()
        self.df_to_mtx()
        self.save_mtx()

    def get_csv(self, path, csv):

        if not path.endswith(".mid"):
            self.error("The input file should have the .mid extension!")
        try:
            self.csv_lines = pm.midi_to_csv(path)
        except:
            self.error("Could not convert your .mid file to .csv")
        if csv:
            self.csv_path = path.replace(".mid", ".csv")
            with open(self.csv_path, "w") as f:
                f.write("".join(self.csv_lines))
        self.title_author = [l for l in self.csv_lines if l.startswith("1,") and\
            (TITLE in l or TEXT in l)]
        self.csv_lines = [l.replace(" ", "").replace(RC, "") for l in self.csv_lines]
        self.mtx_path = path.replace(".mid", "_converted" + EXTENSION)

    def handle_multivoices(self):

        idx_tracks = np.sort(self.notes.query("on == @ON").idx_csv.unique())
        new_track = np.max(idx_tracks) + 1
        voices = pd.DataFrame(columns=self.notes.columns)
        idx_map = {}
        for idx in idx_tracks:
            notes_idx = self.notes.query("idx_csv == @idx").copy()
            new_idx = idx
            while len(notes_idx) != 0:
                idx_map[new_idx] = idx
                voice = notes_idx.groupby(["time", "on"]).head(1).copy()
                voice["idx_csv"] = new_idx
                voices = voices.append(voice)
                notes_idx = notes_idx.drop(voice.index)
                if len(notes_idx) != 0:
                    new_idx = new_track
                    new_track += 1
        self.notes = voices.reset_index(drop=True)
        self.idx_map = idx_map

    def csv_to_df(self):

        csv = pd.DataFrame(columns=["idx_csv", "time", "type", "value0",\
            "value1", "value2", "value3"], data=[l.split(SEP)\
            for l in self.csv_lines])
        csv = csv.drop_duplicates()
        csv = csv.astype({"idx_csv": np.int8, "time": np.int32})
        self.end = csv.time.max() - 1
        self.ticks = int(csv.query("type == @HEADER").value2.values[0])
        unused_types = [START, END, HEADER, EOF, PROGRAM, CONTROL, COPYRIGHT]
        csv = csv.query("type not in @unused_types")
        meta = csv.query("type not in [@NOTEON, @NOTEOFF]")
        meta = meta.drop(columns=["value2", "value3"])
        self.meta = meta.sort_values(by=["idx_csv", "time"])
        notes = csv.query("type in [@NOTEON, @NOTEOFF]")
        notes = notes.drop(columns=["value0", "value3"])
        notes = notes.rename(columns={
            "type"      :   "on",
            "value1"    :   "pitch",
            "value2"    :   "volume"})
        notes["on"] = notes["on"].map({NOTEON: ON, NOTEOFF: OFF})
        notes = notes.astype({"on": np.int8, "pitch": np.int8, "volume": np.int8})
        self.notes = notes.sort_values(by=["idx_csv", "time", "on", "pitch"])
        self.handle_multivoices()

    def df_to_mtx(self):

        self.get_updates()
        self.get_meta()
        self.get_struct()
        self.get_bars()

    def mtx_append(self, l):
        if type(l) == str:
            self.mtx_lines.append(l + RC)
        elif type(l) == list:
            for item in l:
                self.mtx_append(item)

    def parse_ts(self):

        ts = self.meta.query("type == @TIME_SIGNATURE")
        ts = ts.drop(columns=["idx_csv", "type", "value1"])
        ts = ts.rename(columns={"value0": "beats"})
        ts = ts.astype({"beats": np.int8})
        ts["bar_length"] = ts["beats"] * self.ticks
        if len(ts.query("time == 0")) == 0:
            self.error("You didn't set the time signature at the beginning!")
        self.ts = ts.reset_index(drop=True)

    def parse_key(self):

        key = self.meta.query("type == @KEY_SIGNATURE")
        key = key.drop(columns=["idx_csv", "type"])
        key = key.rename(columns={"value0": "key_n", "value1": "key_type"})
        key = key.astype({"key_n": np.int8})
        key["name"], key["note"], key["minor"] = None, None, None
        for _, row in key.iterrows():
            keys_to_use, shift, origin = (FLAT_KEYS, 6, -1)\
                if row.key_n < 0 else (SHARP_KEYS, 8, 0)
            key_index = origin + abs(row.key_n) +\
                (shift if row.key_type == MINOR else 0)
            key_name = keys_to_use[key_index]
            key.at[_, "name"] = key_name
        key["note"] = key["name"].apply(key2note)
        key["minor"] = key["name"].apply(isminor)
        key = key.drop(columns=["key_n", "key_type"])
        if len(key.query("time == 0")) == 0:
            self.error("You didn't set the key at the beginning!")
        self.key = key.reset_index(drop=True)

    def parse_tempo(self):

        tempo = self.meta.query("type == @TEMPO")
        tempo = tempo.drop(columns=["idx_csv", "type", "value1"])
        tempo = tempo.rename(columns={"value0": "bpm"})
        tempo = tempo.astype({"bpm": np.int32})
        tempo["bpm"] = tempo.bpm.apply(lambda x: round(BPM_TO_TEMPO / x))
        [istempo(t) for t in tempo.bpm.values]
        if len(tempo.query("time == 0")) == 0:
            print("Since you didn't set the tempo at the beginning, " +\
                "we chose tempo = 80 bpm")
            tempo = tempo.append({
                "time"  :   np.int8(0),
                "bpm"   :   np.int32(80)
            }, ignore_index=True)
        self.tempo = tempo.reset_index(drop=True)

    def get_updates(self):

        self.parse_ts()
        self.parse_key()
        self.parse_tempo()

    def get_meta(self):

        idx_tracks = list(self.idx_map.keys())
        n_tracks = len(idx_tracks)
        if n_tracks < 1:
            self.error("There should be at least one instrument in your " +\
                "input .mid file")
        title = [l for l in self.title_author if TITLE in l]
        title = "Untitled" if len(title) == 0 else title[-1]\
            .split(SEP)[3].replace("\"", "").replace(RC, "")
        author = [l for l in self.title_author if TEXT in l]
        author = "Unknown" if len(author) == 0 else author[-1]\
            .split(SEP)[3].replace("\"", "").replace(RC, "")
        volume = 50
        instruments = self.meta.query("type == @TITLE and idx_csv > 1")
        instruments = instruments.drop(columns=["time", "type", "value1"])
        instruments = instruments.rename(columns={"value0": "full_name"}).reset_index(drop=True)
        instruments["full_name"] = instruments["full_name"].str.lower()
        for item, value in INSTRUMENTS_NAMES_REPLACE_MAP.items():
            instruments["full_name"] = instruments["full_name"].str.replace(item, value)
        tracks = pd.DataFrame(columns=["idx_csv", "instrument", "name", "isdrum", "highpitch"])
        for idx_track in idx_tracks:
            idx_csv = self.idx_map[idx_track]
            instrument = instruments.query("idx_csv == @idx_csv").full_name.values[0]
            name = INSTRUMENTS[instrument]["shortname"]
            isdrum = 1 if instrument == "drums" else 0
            if isdrum:
                highpitch = 0
            else:
                highnote = INSTRUMENTS[instrument]["highnote"]
                highpitch = note2pitch(highnote[:-1], int(highnote[-1]))
            tracks = tracks.append({
                "idx_csv"       :   idx_track,
                "instrument"    :   instrument,
                "name"          :   name,
                "isdrum"        :   isdrum,
                "highpitch"     :   highpitch,
            }, ignore_index=True)
        for _, g in tracks.groupby("instrument"):
            if len(g) != 1:
                name = g["name"].values[0]
                name = np.array([name + str(i + 1) for i in range(len(g))])
                tracks.at[g.index, "name"] = name
        self.tracks = tracks
        ts = str(self.ts.query("time == 0").beats.values[-1]) + "/4"
        key = self.key.query("time == 0").name.values[-1]
        tempo = str(self.tempo.query("time == 0").bpm.values[-1])
        self.mtx_lines = []
        self.mtx_append(METADATA + RC)
        self.mtx_append("tempo = " + tempo)
        self.mtx_append("key = " + key)
        self.mtx_append("time_signature = " + ts)
        self.mtx_append("instruments = " + ", ".join(tracks.instrument.values))
        self.mtx_append("names = " + ", ".join(tracks.name.values))
        self.mtx_append("title = " + title)
        self.mtx_append("author = " + author)
        self.mtx_append("volume = " + str(volume))

    def parse_pitch(self, row):

        degree = ""
        shift = int(row["shift"])
        shifts = MINOR_SHIFTS if row.minor else MAJOR_SHIFTS
        if shift not in shifts:
            shift -= 1
            degree = SHARP
        shift = shifts.index(shift) + 1
        return str(shift) + degree + row.octave

    def get_struct(self):

        self.notes.loc[self.notes.on == OFF, "time"] -= 1
        self.ts = self.ts.append({"time": self.end, "beats": 0, "bar_length": 0},\
            ignore_index=True)
        self.ts["n_bars"] = (self.ts.time.diff(-1).abs()) / self.ticks
        self.ts = self.ts.dropna()
        self.ts["n_bars"] = self.ts.n_bars / self.ts.beats
        self.ts["n_bars"] = self.ts.n_bars.apply(np.ceil).astype(np.int)
        self.ts["first_bar"] = self.ts.n_bars.cumsum()
        self.ts.loc[1:, "first_bar"] = self.ts["first_bar"].values[:-1]
        self.ts.loc[0, "first_bar"] = 0
        for _, row in self.ts.iterrows():
            self.notes.loc[self.notes.time >= row.time, "bar_length"] = row.bar_length
            self.notes.loc[self.notes.time >= row.time, "bar"] = row.first_bar + ((self.notes.time - row.time) // self.notes.bar_length)
            self.notes.loc[self.notes.time >= row.time, "bar_start"] = row.time + ((self.notes.time - row.time) // self.notes.bar_length) * self.notes.bar_length
        self.notes["bar_stop"] = self.notes.bar_start + self.notes.bar_length
        self.notes.bar += 1
        for _, row in self.tempo.iterrows():
            self.notes.loc[self.notes.time >= row.time, "tempo"] = int(row.bpm)
        self.notes = self.notes.astype({"tempo": np.int32})
        for _, row in self.key.iterrows():
            for idx_csv in self.notes.idx_csv.unique():
                time = row.time
                highpitch = self.tracks.query("idx_csv == @idx_csv").iloc[0].highpitch
                if highpitch == 0:
                    default_pitch = 0
                else:
                    default_pitch = max([p for p in PITCHES[row.note] if p <= highpitch])
                track_key_index = self.notes.query("idx_csv == @idx_csv and time >= @time").index
                self.notes.loc[track_key_index, "default_pitch"] = default_pitch
            self.notes.loc[self.notes.time >= row.time, "minor"] = row.minor

        # TIED NOTES #
        self.notes["tied"] = ""
        self.notes["max_time"] = self.notes.groupby(["idx_csv", "bar"])\
            ["time"].transform("max")
        tied_bars = self.notes.query("max_time == time and on == @ON")
        self.notes.loc[tied_bars.index, "tied"] = INTRA_SEP + TIED
        for _, row in tied_bars.iterrows():
            note_off = self.notes.loc[row.name + 1]
            n_tied_bars = int(np.ceil((note_off.time - row.bar_stop) / row.bar_length))
            for i_tied_bar in range(n_tied_bars):
                note_off_till_end = row.copy()
                note_off_till_end.time = int(row.bar_stop - 1 + (row.bar_length * i_tied_bar))
                note_off_till_end.on = OFF
                note_off_till_end.bar = int(row.bar + i_tied_bar)
                note_off_till_end.bar_start = int(row.bar_stop + (row.bar_length * i_tied_bar))
                self.notes = self.notes.append(note_off_till_end, ignore_index=True)
                if i_tied_bar != n_tied_bars - 1:
                    note_on_next_bar = row.copy()
                    note_on_next_bar.time = int(row.bar_stop + (row.bar_length * i_tied_bar))
                    note_on_next_bar.on = ON
                    note_on_next_bar.bar = int(row.bar + 1 + i_tied_bar)
                    note_on_next_bar.bar_start = int(row.bar_stop + (row.bar_length * i_tied_bar))
                    note_on_next_bar.tied = INTRA_SEP + TIED
                else:
                    note_on_next_bar = note_off.copy()
                    note_on_next_bar.time = int(row.bar_stop + (row.bar_length * i_tied_bar))
                    note_on_next_bar.on = ON
                    note_on_next_bar.bar = int(row.bar + 1 + i_tied_bar)
                    note_on_next_bar.bar_start = int(row.bar_stop + (row.bar_length * i_tied_bar))
                    note_on_next_bar.pitch = row.pitch
                    note_on_next_bar.volume = row.volume
                    if note_off.on == ON:
                        note_off_till_end = note_off.copy()
                        time = note_off.time
                        bar = self.notes.query("time < @time").bar.max()
                        note_off_till_end.time = int(time - 1)
                        note_off_till_end.on = OFF
                        note_off_till_end.bar = int(bar)
                        note_off_till_end.bar_start = int(row.bar_stop + (row.bar_length * (1 + i_tied_bar)))
                        note_off_till_end.pitch = row.pitch
                        note_off_till_end.volume = row.volume
                        self.notes = self.notes.append(note_off_till_end, ignore_index=True)
                self.notes = self.notes.append(note_on_next_bar, ignore_index=True)

        # SILENCE AT BEGINNING OF BAR #
        self.notes["min_time"] = self.notes.groupby(["idx_csv", "bar"])\
            ["time"].transform("min")
        beginning_idx = self.notes.query("min_time == time and min_time != bar_start").index
        self.notes["beginning_length"] = (self.notes.min_time - self.notes.bar_start) / self.ticks
        self.notes["beginning"] = ""
        self.notes.loc[beginning_idx, "beginning"] = self.notes.beginning_length.apply(\
            lambda x: INTRA_SEP + (REVERSED_LENGTH[x]\
            if x in REVERSED_LENGTH.keys() else str(int(x*(10**LENGTH_RESOLUTION))/(10**LENGTH_RESOLUTION))) + SEP + " ")
        self.notes = self.notes.drop(columns=["bar_length", "bar_start", "bar_stop",\
            "max_time", "min_time", "beginning_length"])
        self.notes = self.notes.sort_values(by=["idx_csv", "time", "on"]).reset_index(drop=True)

        # PARSE LENGTH #
        self.notes.loc[self.notes.on==OFF, "time"] += 1
        self.notes["diff"] = self.notes.groupby(["idx_csv", "bar"]).time.diff(-1).abs() / self.ticks
        self.notes = self.notes.query("diff != 0").dropna()
        self.notes["length"] = self.notes["diff"].apply(lambda x:\
            INTRA_SEP + (REVERSED_LENGTH[x] if x in REVERSED_LENGTH.keys()\
                else str(int(x*(10**LENGTH_RESOLUTION))/(10**LENGTH_RESOLUTION))))

        # PARSE PITCH #
        self.notes["pitch"] = self.notes.pitch - self.notes.default_pitch
        self.notes["isdrum"] = False
        drums_list = self.tracks.query("isdrum == 1")["idx_csv"].values
        drums_idx = self.notes.query("idx_csv in @drums_list").index
        self.notes.loc[drums_idx, "isdrum"] = True
        for _, row in self.tracks.iterrows():
            self.notes.loc[self.notes.idx_csv == row.idx_csv, "name"] =\
                row["name"]
        self.notes["octave"] = self.notes["pitch"] // N_NOTES_IN_OCTAVE
        self.notes["octave"] = self.notes["octave"].astype(np.int8).apply(lambda n:\
            n * OCTAVE_UP if n >= 0 else (-n)*OCTAVE_DOWN)
        self.notes["shift"] = self.notes["pitch"] % N_NOTES_IN_OCTAVE
        self.notes["degree"] = self.notes[["shift", "minor", "octave"]].apply(self.parse_pitch, axis=1)
        self.notes.loc[self.notes.isdrum == True, "degree"] =\
            self.notes.loc[self.notes.isdrum == True, "pitch"].map(REVERSED_DRUMS)
        self.notes.loc[self.notes.on == OFF, "degree"] = ""

        # PARSE VOLUME #
        self.notes.loc[self.notes.on == ON, "volume"] = self.notes["volume"].divide(\
            DYNNAMICS_DOWNSAMPLE).apply(np.floor)
        self.notes["volume_diff"] = 0
        self.notes.loc[self.notes.on == ON, "volume_diff"] =self.notes.loc[self.notes.on == ON, "volume"].diff()
        self.notes["volume_diff"] = self.notes.volume_diff.fillna(1)
        self.notes.volume = self.notes.volume.apply(\
            lambda x: INTRA_SEP + DYNAMICS[x])
        volume_idx = self.notes.query("volume_diff == 0").index
        self.notes.loc[volume_idx, "volume"] = ""

        # NOTES #
        self.notes.bar = self.notes.bar.astype(np.int16)
        self.notes["note"] = self.notes["beginning"] + self.notes["degree"] +\
            self.notes["length"] + self.notes["volume"] + self.notes["tied"] + SEP + " "

    def get_bars(self):

        updates = {}
        for _, row in self.tempo.iterrows():
            time = row.time
            bar_name = NEW_BAR + str(self.notes.query("time <= @time").bar.max())
            if bar_name != "bar1" and bar_name not in updates.keys():
                updates[bar_name] = BAR_UPDATE_SYMBOL +\
                    " tempo = " + str(int(row.bpm)) + ", "
        for _, row in self.key.iterrows():
            time = row.time
            bar_name = NEW_BAR + str(self.notes.query("time <= @time").bar.max())
            if bar_name != "bar1" and bar_name not in updates.keys():
                updates[bar_name] = BAR_UPDATE_SYMBOL +\
                    " key = " + row.key_name + ", "
            elif bar_name != "bar1":
                updates[bar_name] += "key = " + row["name"] + ", "
        for _, t in self.ts.iterrows():
            time = t.time
            bar_name = NEW_BAR + str(self.notes.query("time <= @time").bar.max())
            if bar_name != "bar1" and bar_name not in updates.keys():
                updates[bar_name] = BAR_UPDATE_SYMBOL +\
                    " time_signature = " + str(int(t.beats)) + "/4, "
            elif bar_name != "bar1":
                updates[bar_name] += "time_signature = " + str(int(t.beats)) + "/4, "
        for u in updates.keys():
            updates[u] = updates[u][:-2]

        self.mtx_append(RC + BARS)
        bar_list = []
        for _, bar in self.notes.groupby("bar"):
            bar_name = NEW_BAR + str(bar.bar.values[0])
            bar_list.append(bar_name)
            self.mtx_append(RC + bar_name)
            if bar_name in updates.keys():
                self.mtx_append(updates[bar_name])
            for _, instr in bar.groupby("idx_csv"):
                name = instr.name.values[0]
                self.mtx_append(name + " = " + instr.note.sum()[:-2])

        self.mtx_append(RC + STRUCTURE)
        output_line = RC + "output = " + ", ".join(bar_list)
        self.mtx_append(output_line)

    def save_mtx(self):

        with open(self.mtx_path, "w") as output_file:
            output_file.write("".join(self.mtx_lines))

