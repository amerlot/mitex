#!/bin/sh

echo "$@"
echo "$1 $2"

gs -dNOPAUSE -sPAPERSIZE=a4 -sDEVICE=pdfwrite -sOUTPUTFILE="joined.pdf" -dBATCH "$@"
rm $@
mv joined.pdf $1