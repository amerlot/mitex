#!/bin/sh
# Convert PostScript to PDF without specifying CompatibilityLevel.

# This definition is changed on install to match the
# executable name set in the makefile

if [ $# -lt 1 -o $# -gt 2 ]; then
	echo "Usage: `basename \"$0\"` (input.[e]ps|-) [output.pdf|-]" 1>&2
	exit 1
fi

infile="$1"
outfile="$2"

# We have to include the options twice because -I only takes effect if it
# appears before other options.
gs -P- -dSAFER -q -P- -dNOPAUSE -dBATCH -sPAPERSIZE=a4 -sDEVICE=pdfwrite -sstdout=%stderr "-sOutputFile=$outfile" -P- -dSAFER -c .setpdfwrite -f $infile